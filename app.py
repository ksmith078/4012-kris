from flask import Flask, render_template, url_for, request, session, redirect
#from flask_pymongo import PyMongo
#import bcrypt
import os

app = Flask(__name__)
app.config['DEBUG'] = True
#client = MongoClient(os.environ['MONGODB_HOST'], 27017)
#mongo = PyMongo(app)
#db = client.db

@app.route('/')
def home():
  return render_template("x.html", appName = "x")

app.run(host="0.0.0.0", debug=True)
