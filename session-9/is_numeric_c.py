def is_numeric_c(string):
    if string.isdigit():
        return True
    else:
        try:
            float(string)
        except:
            return False
        else:
            return True

tests = ['foobar', '123', '12345', '1.23', '.1.2.3']

for i in tests:
    print(is_numeric_c(i))
